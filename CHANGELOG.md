## 2.3.4 (2024-12-07)

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/611d4b5dcbea8c078561b2529e1603bd490f3792) by @fctaddia\
@types/node -> 22.10.1\
typescript -> 5.7.2

## 2.3.3 (2024-10-19)

### Bug fixes

- [Fix retry limit to 10 for 5xx errors](https://gitlab.com/kenble/fcm-http2/-/commit/e5c9fb2fa7a3ca0bc33d7e578084d77b19b0fa66) by @fctaddia

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/1d4a7722f012546befd7dc616ae0663a821e96b8) by @fctaddia\
@types/node -> 22.7.4\
typescript -> 5.6.3

## 2.3.2 (2024-10-03)

### Bug fixes

- [Fix retry on FCM HTML Error 502 (Server Error)](https://gitlab.com/kenble/fcm-http2/-/commit/5ae5f6bbcf8d1105812d4a5485993c21bf086185) by @fctaddia

## 2.3.0 (2024-09-29)

### Bug fixes

- [Always enabled retry if the FCM service is 5xx error](https://gitlab.com/kenble/fcm-http2/-/commit/787cc98740a5f89d567268732fa95016cfbe978b) by @fctaddia

### Features

- [Centralized verification and recovery serviceAccount in the FcmOptions class](https://gitlab.com/kenble/fcm-http2/-/commit/9f348640cfaeac20427ee32372aa82a586833a89) by @fctaddia
- [Removed unnecessary variable swapping in methods and used the fcmOptions object directly](https://gitlab.com/kenble/fcm-http2/-/commit/ba1cdacb5797697074ad173697eb74d2436e65ca) by @fctaddia

## 2.2.2 (2024-09-28)

### Bug fixes

- [Improved handling of error logs when creating a new FCM client](https://gitlab.com/kenble/fcm-http2/-/commit/fb0beebf3f556b300f5ded481dd5b51168c6b63a) by @fctaddia
- [Always enabled retry if the FCM service is 503 Service Unvaiable](https://gitlab.com/kenble/fcm-http2/-/commit/b6a6028d87f84a4c1ccd9b5f4f5fdb658c3ee740) by @fctaddia
- [Fix for not logging lastStreamId in FCM HTTP2 GOAWAY](https://gitlab.com/kenble/fcm-http2/-/commit/15fa0929a938ba1e10dbebce86f41dea0849676f) by @fctaddia

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/4357a96f60afb6ed93b08918374f7a33c63d2c7b) by @fctaddia\
@types/node -> 22.7.4

## 2.2.1 (2024-09-23)

### Bug fixes

- [Fix maxConcurrentStreamsAllowed in FCM class constructor](https://gitlab.com/kenble/fcm-http2/-/commit/38ceb13e4fea3d6b96c50a3da629c4a0277f00ff) by @fctaddia - [#4](https://gitlab.com/kenble/fcm-http2/-/issues/4)

## 2.2.0 (2024-09-23)

### Bug fixes

- [Implemented logic to retry when the FCM client is invalid](https://gitlab.com/kenble/fcm-http2/-/commit/ffe4ac5874f3687fd69721fe3c4e2b9a33f8782e) by @fctaddia - [#3](https://gitlab.com/kenble/fcm-http2/-/issues/3)

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/da72e85646448ab285ec7a5b108599ab3285cab5) by @fctaddia\
googleapis -> 144.0.0\
@types/node -> 22.5.5\
typescript -> 5.6.2

## 2.1.6 (2024-08-22)

### Bug fixes

- [Set HTTP/2 client peerMaxConcurrentStreams to maxConcurrentStreamsAllowed](https://gitlab.com/kenble/fcm-http2/-/commit/c125d88d758e54ee2cdcaf920e16e8d75dae5499) by @fctaddia
- [Switch to async/each instead of async/eachLimit to speed up delivery](https://gitlab.com/kenble/fcm-http2/-/commit/bb59ce46cbee6dfe2ce61fdc6b4e438456dd2fbf) by @fctaddia

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/2a9eaa1b8d9fd548c13fe9a3f4b8af77ac6932f4) by @fctaddia\
async -> 3.2.6\
googleapis -> 142.0.0\
@types/node -> 22.5.0\
typescript -> 5.5.4

## 2.1.4 (2024-07-17)

### Bug fixes

- [Fixed tokens marked as unregistered when the message payload is invalid and added support to INVALID_ARGUMENT](https://gitlab.com/kenble/fcm-http2/-/commit/a7190f075104649e4bde448b33f1c018cb61d445) by @fctaddia

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/4ae2457322b07747ff39d4d60f35161926510d88) by @fctaddia\
@types/node -> 20.14.11

## 2.1.3 (2024-07-03)

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/991dfcf148049eda8cbd803f5593271415712d50) by @fctaddia\
googleapis -> 140.0.1\
@types/node -> 20.14.9\
typescript -> 5.5.3

## 2.1.2 (2024-06-23)

### Bug fixes

- [Using the Spread Operator to create a copy of the token array instead of referencing the same elements](https://gitlab.com/kenble/fcm-http2/-/commit/e3ed347b31b102148a84c86e8c1165a0aa4f4c7c) by @fctaddia

## 2.1.1 (2024-06-22)

### Bug fixes

- [Add retry on status code >= 500](https://gitlab.com/kenble/fcm-http2/-/commit/03344e2b2dd6750fac6bdea8ed2add248ff06afa) by @fctaddia

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/29422da1031918d1b60a70fa6ae686753ea88a4c) by @fctaddia\
@types/node -> 20.14.8

## 2.1.0 (2024-06-21)

### Performance improvements

- [Code Optimization for Multicast Sending](https://gitlab.com/kenble/fcm-http2/-/commit/492e67e5edc021a6d7461662aea854570296d41b) by @fctaddia
- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/2b59425df453954dd7e8bf587559606300603ec3) by @fctaddia\
@types/node -> 20.14.7

## 2.0.7 (2024-06-20)

### Bug fixes

- [Added "message" and "tokens" validity check for "sendMulticast" method](https://gitlab.com/kenble/fcm-http2/-/commit/6abb2068cd72a2dfff0dfc4258fffc05f361d702) by @fctaddia - [#1](https://gitlab.com/kenble/fcm-http2/-/issues/1)

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/de327c15836b68aed197829bfa6d49e9bf41968f) by @fctaddia\
@types/node -> 20.14.6\
typescript -> 5.5.2

## 2.0.6 (2024-06-18)

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/a98142a5c0bd98e732e712b174c9aca396764734) by @fctaddia\
googleapis -> 140.0.0\
@types/node -> 20.14.4

## 2.0.5 (2024-04-28)

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/841d07f407301ecb3ccc47f65f2211ce7097e610) by @fctaddia\
googleapis -> 135.0.0\
@types/node -> 20.12.7\
typescript -> 5.4.5

## 2.0.4 (2024-03-27)

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/1d4d9883eadce8af975279ecc8bea2497fbd3780) by @fctaddia\
googleapis -> 134.0.0\
@types/node -> 20.11.30\
typescript -> 5.4.3

## 2.0.3 (2024-03-08)

### Performance improvements

- [Updated dependencies](https://gitlab.com/kenble/fcm-http2/-/commit/404dc905a5a3da80b4ab5bcbf048361b1e8482b4) by @fctaddia\
googleapis -> 133.0.0\
@types/node -> 20.11.25\
typescript -> 5.4.2

## 2.0.0 (2024-01-24)

### Features

- [Implemented Typescript logic for sending a multicast notification via Firebase Cloud Messaging](https://gitlab.com/kenble/fcm-http2/-/commit/8e3b325c0748da3ef4caab1a8499783b2ab64074) by @fctaddia

## 1.0.1 (2024-01-14)

### Features

- [Added the ability to specify the fcmOptions object](https://gitlab.com/kenble/fcm-http2/-/commit/1528cafffd9ba9e6cfcac6f4d27cd44014c34722) by @fctaddia

## 1.0.0 (2024-01-13)

### Features

- [Created methods to send multicast notifications using HTTP/2 and FCM HTTP v1 API](https://gitlab.com/kenble/fcm-http2/-/commit/b2ca8e11a8d67a3b278076866a629a3f03183e53) by @fctaddia
