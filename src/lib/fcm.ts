import async from 'async';
import http2 from 'http2';
import { google } from 'googleapis';
import { FcmClient } from '../entity/fcm-client';
import { FcmOptions } from '../entity/fcm-options';

/**
 * Class for sending a notification to multiple devices using HTTP/2 multiplexing.
 *
 * @author Kenble - f.taddia
 */
export class FCM {

    /**
     * Options to manage calls to Firebase Cloud Messaging APIs.
     * @type {FcmOptions}
     */
    private fcmOptions: FcmOptions;


    /**
     * Constructor to initialize FCM options.
     *
     * @author Kenble - f.taddia
     * @param options : FCM options, if undefined the default ones are used and the service-account.json
     * file present in the library directory is read
     */
    constructor(options?: FcmOptions) {
        this.fcmOptions = new FcmOptions(options);
    }


    /**
     * Send a notification to multiple devices using HTTP/2 multiplexing.
     *
     * @author Kenble - f.taddia
     * @param message to send
     * @param tokens to send the notification to
     * @returns returns the list of tokens not registered in fcm
     */
    sendMulticast(message: object, tokens: Array<string>): Promise<Array<string>> {

        // Promisify method
        return new Promise<Array<string>>((resolve, reject) => {

            // Check the validity of the message
            if (!message) {
                reject(new Error('Message is mandatory'));
            }

            // Check the validity of the token array
            if (tokens?.length <= 0) {
                reject(new Error('Token array is mandatory'));
            }

            // Prepare batches array
            const tokenBatches: Array<Array<string>> = [];

            // Calculate max devices per batch
            let batchLimit = Math.ceil(tokens.length / this.fcmOptions.maxConcurrentConnections);

            // Use just one batch/HTTP2 connection if batch limit is less than maxConcurrentStreamsAllowed
            if (batchLimit <= this.fcmOptions.maxConcurrentStreamsAllowed) {
                batchLimit = this.fcmOptions.maxConcurrentStreamsAllowed;
            }

            // Traverse tokens and split them up into batches of X devices each  
            for (let start = 0; start < tokens.length; start += batchLimit) {
                tokenBatches.push([...tokens.slice(start, start + batchLimit)]);
            }

            // Keep track of unregistered device tokens
            const unregisteredTokens: Array<string> = [];

            // Get OAuth2 token
            this.getAccessToken().then(accessToken => {

                // Count batches to determine when all notifications have been sent
                let done = 0;

                // Send notification using HTTP/2 multiplexing
                tokenBatches.forEach(tokenBatch => {

                    // Send notification to current token batch
                    this.processBatch(message, tokenBatch, accessToken).then((unregisteredTokensList: Array<string>) => {

                        // Add unregistred tokens (if any)
                        if (unregisteredTokensList.length > 0) {
                            unregisteredTokens.push(...unregisteredTokensList);
                        }

                        // Done with this batch
                        done++;

                        // If all batches processed, resolve final promise with list of unregistred tokens
                        if (done === tokenBatches.length) {
                            resolve(unregisteredTokens);
                        }

                    }).catch(err => {
                        // Reject promise with error
                        reject(err);
                    });

                });

            }).catch(err => {
                // Failed to generate OAuth2 token
                // most likely due to invalid credentials provided
                reject(err);
            });

        });

    }

    /**
     * OAuth2 access token generation method.
     *
     * @author Kenble - f.taddia
     * @returns returns the access token
     */
    private getAccessToken() {

        // Promisify method
        return new Promise<string>((resolve, reject) => {

            // Create JWT client with Firebase Messaging scope
            const jwtClient = new google.auth.JWT(
                this.fcmOptions.serviceAccount.client_email,
                undefined,
                this.fcmOptions.serviceAccount.private_key,
                this.fcmOptions.scopes,
                undefined
            );

            // Request OAuth2 token
            jwtClient.authorize((err, tokens) => {
                if (err) {
                    // Reject on error
                    return reject(err);
                }
                // Resolve promise with accss token
                resolve(tokens?.access_token!!);
            });

        });

    }

    /**
     * Creation of the HTTP2 client useful for using the FCM API.
     *
     * @author Kenble - f.taddia
     * @returns returns the successfully created client
     */
    private createFcmClient(): Promise<FcmClient> {

        // Promisify method
        return new Promise<FcmClient>(resolve => {

            // Create an HTTP2 client and connect to FCM API
            const client: FcmClient = http2.connect(this.fcmOptions.host, {
                peerMaxConcurrentStreams: this.fcmOptions.maxConcurrentStreamsAllowed
            });

            // Log connection goaway
            client.on('goaway', (err, _lastStreamId, opaqueData) => {
                // Log goaway
                console.error('FCM HTTP2 GOAWAY', err, opaqueData ? opaqueData.toString('utf-8') : null);
            });

            // Listen for connection errors
            client.on('error', err => {
                // Log temporary connection errors to console (retry mechanism inside sendRequest will take care of retrying)
                console.error('FCM HTTP2 Error', err);
            });

            // Listen for socket errors
            client.on('socketError', err => {
                // Notify developer of socket error
                console.error('FCM HTTP2 Socket Error', err);
            });

            // Keep track of unregistered device tokens
            client.unregisteredTokens = [];

            resolve(client);

        });

    }

    /**
     * Sends notifications to a batch of tokens using HTTP/2.
     *
     * @author Kenble - f.taddia
     * @param message to notify
     * @param devices : list of devices to send the notification to
     * @param accessToken : access token for fcm api
     * @returns returns the list of tokens not registered in fcm
     */
    private async processBatch(message: any, devices: Array<string>, accessToken: string) {

        // Promisify method
        return new Promise<Array<string>>((resolve, reject) => {

            // Create new HTTP/2 client for this batch
            this.createFcmClient().then((client: FcmClient) => {

                // Use async/eachLimit to iterate over device tokens
                async.eachLimit(devices, this.fcmOptions.maxConcurrentStreamsAllowed, (device, doneCallback) => {
                    // Create a HTTP/2 request per device token
                    this.sendRequest(client, device, message, accessToken, doneCallback, 0);
                }, err => {
                    // All requests completed, close the HTTP2 client
                    client.close();

                    if (err) {
                        // Reject on error
                        return reject(err);
                    }

                    // Resolve the promise with list of unregistered tokens
                    resolve(client.unregisteredTokens!!);
                });

            });

        });

    }

    /**
     * Verifying the validity of the FCM client.
     *
     * @author Kenble - f.taddia
     * @param client : http2 client with which to call the fcm api
     */
    private async checkClient(client: FcmClient) {
        // Verify that HTTP2 client is invalid
        if (!client || client.closed || client.destroyed) {
            // Verify that retryClient is invalid
            if (!this.fcmOptions.retryClient || this.fcmOptions.retryClient.closed || this.fcmOptions.retryClient.destroyed) {
                try {
                    // Try closing old client and freeing up resources
                    this.fcmOptions.retryClient?.close();
                    this.fcmOptions.retryClient?.destroy();
                } catch (err) {
                    // Ignore errors
                    console.error('[FCM] checkClient error', err);
                }
                // Create new client
                this.fcmOptions.retryClient = await this.createFcmClient();
            }
            // Use retry client as client
            client = this.fcmOptions.retryClient!;
        }
    }

    /**
     * Sends a single notification over an existing HTTP/2 client.
     *
     * @author Kenble - f.taddia
     * @param client : http2 client with which to call the fcm api
     * @param device : device to send the notification to
     * @param message to send
     * @param accessToken : access token for fcm api
     * @param doneCallback : callback when sending is finished
     * @param tries : number of attempts made for a failed send, maximum number of attempts is 3
     */
    private async sendRequest(client: FcmClient, device: string, message: any, accessToken: string, doneCallback: any, tries: number) {

        // Check that the client is still valid, otherwise create a new one and destroy the old one
        await this.checkClient(client);

        // Create a HTTP/2 request per device token
        const request = client.request({
            ':method': 'POST',
            ':scheme': 'https',
            ':path': `/v1/projects/${this.fcmOptions.serviceAccount.project_id}/messages:send`,
            Authorization: `Bearer ${accessToken}`
        });

        // Set encoding as UTF8
        request.setEncoding('utf8');

        // Clone the message object
        const clonedMessage = Object.assign({}, message);

        // Assign device token for the message
        clonedMessage.token = device;

        // Send the request body as stringified JSON
        request.write(
            JSON.stringify({
                // validate_only: true, // Uncomment for dry run
                message: clonedMessage
            })
        );

        // Buffer response data
        let data = '';

        // Add each incoming chunk to response data
        request.on('data', chunk => data += chunk);

        // Keep track of whether we are already retrying this method invocation
        let retrying = false;

        // this
        const me = this;

        // Define error handler
        const errorHandler = (err: any) => {

            // Retry up to 3 times (10 times for FCM 5xx errors)
            if (tries <= 3 || (tries < 10 && ((err && err.code >= 500 && err.code < 600) || (data && data.includes('Server Error'))))) {

                // Avoid retrying twice for the same error
                if (retrying) {
                    return;
                }

                // Keep track of whether we are already retrying in this context
                retrying = true;

                // Retry request using same HTTP2 session in 1 seconds
                return setTimeout(() => { me.sendRequest(client, device, message, accessToken, doneCallback, tries) }, 1 * 1000);

            }

            // Log this
            console.log(`[FCM] Can't retry request (ran out of retries): (data: ${data})`, err, data);

            // Log response data in error
            err.data = data;

            // Even if request failed, mark request as completed as we've already retried 3 times
            doneCallback(err);
        }

        // Response received in full
        request.on('end', () => {
            try {
                // Convert response body to JSON object
                const response = JSON.parse(data);

                // Error?
                if (response.error) {
                    // App uninstall or invalid token?
                    if ((response.error.details && response.error.details[0].errorCode === 'UNREGISTERED') ||
                        (response.error.code === 400 && response.error.status === 'INVALID_ARGUMENT'
                            && response.error.message.includes('not a valid FCM registration token'))) {

                        // Add to unregistered tokens list
                        client.unregisteredTokens?.push(device);
                    
                    // 5xx errors?
                    } else if (response.error.code >= 500 && response.error.code < 600) {
                        // Retry request
                        return errorHandler(response.error);
                    } else {
                        // Call async done callback with unexpected error
                        return doneCallback(response.error);
                    }
                }

                // Mark request as completed
                doneCallback();
            } catch (err) {
                // Invoke error handler with retry mechanism
                errorHandler(err);
            }
        });

        // Log request errors
        request.on('error', err => {
            // Invoke error handler with retry mechanism
            errorHandler(err);
        });

        // Increment tries
        tries++;

        // Send the current request
        request.end();

    }

}
