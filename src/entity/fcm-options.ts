import { FcmClient } from "./fcm-client";

/**
 * Entity for Firebase Cloud Messaging options.
 *
 * @author Kenble - f.taddia
 */
export class FcmOptions {

    /**
     * Host of Firebase Cloud Messaging APIs.
     * @type {string}
     */
    readonly host: string = 'https://fcm.googleapis.com' as const;

    /**
     * Scope of Firebase Cloud Messaging APIs.
     * @type {Array<string>}
     */
    readonly scopes: Array<string> = ['https://www.googleapis.com/auth/firebase.messaging'] as const;


    /**
     * Object to represent the "service-account.json" file.
     * @type {FcmServiceAccount}
     */
    serviceAccount: FcmServiceAccount;

    /**
     * Max number of concurrent HTTP/2 sessions (connections).
     * @type {number}
     */
    maxConcurrentConnections: number;

    /**
     * Max number of concurrent streams (requests) per session.
     * @type {number}
     */
    maxConcurrentStreamsAllowed: number;

    /**
     * FCM client used to retry failed requests.
     * @type {FcmClient}
     */
    retryClient?: FcmClient;


    constructor(entity?: FcmOptions) {
        this.serviceAccount = this.checkAndGetServiceAccount(entity);
        this.maxConcurrentConnections = entity?.maxConcurrentConnections || 10;
        this.maxConcurrentStreamsAllowed = entity?.maxConcurrentStreamsAllowed || 100;
    }


    /**
     * Verifies the validity of the serviceAccount and retrieves it.
     *
     * @author Kenble - f.taddia
     * @param options : FCM configuration options
     * @returns returns the valid serviceAccount
     */
    private checkAndGetServiceAccount(options?: FcmOptions) {

        // If an object is not specified to value fcmOptions then the service-account.json is retrieved in the lib directory
        const serviceAccount = options ? options.serviceAccount : require('./../lib/service-account.json');

        // The serviceAccount configuration file is required
        if (!serviceAccount) {
            throw new Error('Please provide the service account JSON configuration file.');
        }

        // Ensure we have a project ID
        if (!serviceAccount?.project_id) {
            throw new Error('Unable to determine Firebase Project ID from service account file.');
        }

        // Ensure we have a client email
        if (!serviceAccount?.client_email) {
            throw new Error('Unable to determine Firebase Client Email from service account file.');
        }

        // Ensure we have a private key
        if (!serviceAccount?.private_key) {
            throw new Error('Unable to determine Firebase Private Key from service account file.');
        }

        return serviceAccount;
    }

}

/**
 * Interface to represent the "service-account.json" file.
 * NOTE: Only the variables used were mapped.
 *
 * @author Kenble - f.taddia
 */
interface FcmServiceAccount {

    project_id: string;

    private_key: string;

    client_email: string;

}
